import { Component } from '@angular/core';
import { BlogPost } from './blog-post';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    posts = [
        new BlogPost("First post", "This is the first post of my blog !!"),
        new BlogPost("Second post", "Alright, already a second post !"),
        new BlogPost("Third post", "OMG ! I'm on fire !! Three posts in a row !")
    ];
}
