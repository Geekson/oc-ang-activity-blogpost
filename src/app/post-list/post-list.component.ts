import { Component, Input, OnInit } from '@angular/core';
import { BlogPost } from '../blog-post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @Input() blogPost: BlogPost;

  constructor() { }

  ngOnInit() {
  }

  getColor() {
      if (this.blogPost.loveIts > 0) {
          return 'green';
      }
      else if (this.blogPost.loveIts < 0) {
          return 'red';
      }
      else {
          return 'black';
      }
  }

  onLoveCounts(count: number) {
      this.blogPost.loveIts = this.blogPost.loveIts + count;
  }

}
